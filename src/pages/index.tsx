import axios from "axios";
import { GetServerSideProps, NextPage } from "next";
import Head from "next/head";

import { Layout } from "@/components/Layout";
import { ProductGrid } from "@/components/ProductGrid";

import { HomeProps, Product } from "../types";

const Home: NextPage<HomeProps> = ({ products }) => {
  return (
    <>
      <Layout>
        <main>
          <ProductGrid initialProducts={products} />
        </main>
      </Layout>
    </>
  );
};

export const getServerSideProps: GetServerSideProps<HomeProps> = async () => {
  const { data: products } = await axios.get(
    "https://fakestoreapi.com/products?limit=8"
  );
  const productsWithCurrency = products.map((product: Product) => ({
    ...product,
    currency: "USD",
  }));

  return { props: { products: productsWithCurrency } };
};

export default Home;
