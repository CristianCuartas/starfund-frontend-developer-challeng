import React from "react";
import { NextPage } from "next";
import { AppProps } from "next/app";
import { Provider } from "react-redux";
import { ReactElement } from "react";
import Head from "next/head";

import { Header } from "@/components/Header";
import { Footer } from "@/components/Footer";

import "../styles/globals.css";
import store from "../../store";

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactElement;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

function AppLayout({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page);

  return (
    <Provider store={store}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>Starfund frontend challenge</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Header />
      <div className="container h-custom lg:h-customLg" />
      <main>{getLayout(<Component {...pageProps} />)}</main>
      <Footer />
    </Provider>
  );
}

export default AppLayout;
