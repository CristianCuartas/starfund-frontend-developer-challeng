import React, { FC } from "react";
import { LayoutProps } from "./types";

export const Layout: FC<LayoutProps> = ({ children }) => {
  return <div className="pb-10">{children}</div>;
};
