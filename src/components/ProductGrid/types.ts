import { Product } from "@/types";

export type ProductGridProps = {
  initialProducts: Product[];
};
