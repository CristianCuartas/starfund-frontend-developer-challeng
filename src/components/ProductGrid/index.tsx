import axios from "axios";
import { useSelector } from "react-redux";
import React, { FC, useState, useEffect } from "react";

import { Product } from "@/types";

import { ProductGridProps } from "./types";

import { Spinner } from "../Spinner";

import ProductCard from "../ProductCard";

import { RootState } from "../../../store";

export const ProductGrid: FC<ProductGridProps> = ({ initialProducts }) => {
  const [products, setProducts] = useState<Product[]>(initialProducts);
  const [loading, setLoading] = useState(false);
  const [loadedAll, setLoadedAll] = useState(false);
  const [limit, setLimit] = useState(8);

  const searchValue = useSelector((state: RootState) => state.search.text);
  const sortField = useSelector((state: RootState) => state.sort.field);

  const getSortFunction = (field: string) => {
    if (field === "price") {
      return (a: Product, b: Product) => b.price - a.price;
    }
    if (field === "rating") {
      return (a: Product, b: Product) => b.rating.rate - a.rating.rate;
    }
    return (a: Product, b: Product) => 0;
  };

  const filteredAndSortedProducts = products
    .filter((product) =>
      product.title.toLowerCase().includes(searchValue.toLowerCase())
    )
    .sort(getSortFunction(sortField));

  useEffect(() => {
    const fetchProducts = async () => {
      setLoading(true);
      const { data: response } = await axios.get(
        `https://fakestoreapi.com/products?limit=${limit}`
      );

      const productsWithCurrency = response.map((product: Product) => ({
        ...product,
        currency: "USD",
      }));

      if (productsWithCurrency.length < limit) {
        setLoadedAll(true);
      }
      setProducts(productsWithCurrency);
      setLoading(false);
    };

    if (limit > initialProducts.length) {
      fetchProducts();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [limit]);

  const loadMore = () => {
    if (!loading && !loadedAll) {
      setLimit(limit + 4);
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      if (
        window.innerHeight + document.documentElement.scrollTop !==
        document.documentElement.offsetHeight
      )
        return;
      loadMore();
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading, loadedAll]);

  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
      {filteredAndSortedProducts.map((product) => (
        <ProductCard product={product} key={product.id} />
      ))}
      {loading && <Spinner />}
    </div>
  );
};
