import React, { FC } from "react";
import Link from "next/link";

export const Footer: FC = () => {
  const year = new Date().getFullYear();
  return (
    <footer className="bg-gray-800 p-6 text-center text-white bottom-0 w-full">
      <nav aria-label="Footer navigation">
        <ul className="flex justify-center space-x-4">
          <li>
            <Link
              href="https://www.linkedin.com/in/dexhcz/"
              className="text-white hover:underline"
            >
              /in/dexhcz/
            </Link>
          </li>
        </ul>
      </nav>
      <p>
        &copy; {year} - Front End Developer Challenge. Todos los derechos
        reservados.
      </p>
    </footer>
  );
};
