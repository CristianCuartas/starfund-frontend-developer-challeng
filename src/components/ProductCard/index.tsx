import React, { FC } from "react";
import Image from "next/image";
import StarRatings from "react-star-ratings";
import { FaPlus, FaTrash } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";

import { ProductCardProps } from "./types";

import {
  addToCart,
  isProductInCart,
  removeFromCart,
} from "../../../store/cartSlice";
import { RootState } from "../../../store";

const ProductCard: FC<ProductCardProps> = ({ product }) => {
  const dispatch = useDispatch();
  const productInCart = useSelector((state: RootState) =>
    isProductInCart(state.cart, product.id)
  );

  const handleCartAction = () => {
    if (productInCart) {
      dispatch(removeFromCart(product.id));
    } else {
      dispatch(addToCart(product));
    }
  };

  return (
    <div className="flex flex-col bg-gray-800 border border-gray-700 rounded-xl p-4 h-96 justify-between shadow-lg transform transition duration-500 hover:scale-105">
      <div className="h-1/2 relative mb-4">
        <Image
          src={product.image}
          alt={`Imagen de ${product.title}`}
          layout="fill"
          objectFit="contain"
        />
      </div>
      <div className="flex-grow text-white">
        <h2 className="font-semibold text-lg mb-2">{product.title}</h2>
        <p className="text-sm line-clamp-2 mb-2">
          {product.description.slice(0, 100)}
        </p>
      </div>
      <div>
        <StarRatings
          rating={product.rating.rate}
          starDimension="20px"
          starSpacing="2px"
          starRatedColor="#DEB887"
        />
        <p className="font-semibold text-yellow-300">
          ${product.price} {product.currency}
        </p>

        <button
          onClick={handleCartAction}
          className={`${
            productInCart ? "bg-red-500" : "bg-green-500"
          } text-white px-2 py-1 rounded-md flex items-center justify-center mt-2`}
        >
          {productInCart ? (
            <FaTrash className="mr-1" />
          ) : (
            <FaPlus className="mr-1" />
          )}
          {productInCart ? "Remove from Cart" : "Add to Cart"}
        </button>
      </div>
    </div>
  );
};

export default ProductCard;
