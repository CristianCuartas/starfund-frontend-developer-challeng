import { Product } from "@/types";

export type ProductCardProps = {
  product: Product;
};
