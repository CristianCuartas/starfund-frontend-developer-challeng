import Image from "next/image";
import React, { FC } from "react";
import {
  FaSearch,
  FaFilter,
  FaDollarSign,
  FaShoppingCart,
} from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";

import { RootState } from "../../../store";
import { setSortField } from "../../../store/sortSlice";
import { setSearchText } from "../../../store/searchSlice";

const selectSearchValue = (state: RootState) => state.search.text;
const selectSortField = (state: RootState) => state.sort.field;
const selectCartItemCount = (state: RootState) => state.cart.items.length;
const selectCartTotalPrice = (state: RootState) =>
  state.cart.items.reduce((total, item) => total + item.price, 0);

export const Header: FC = () => {
  const dispatch = useDispatch();

  const sortField = useSelector(selectSortField);
  const searchValue = useSelector(selectSearchValue);
  const itemCount = useSelector(selectCartItemCount);
  const totalPrice = useSelector(selectCartTotalPrice);

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setSearchText(event.target.value));
  };

  const handleSortChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    dispatch(setSortField(e.target.value as "" | "price" | "rating"));
  };

  return (
    <header className="fixed w-full z-10 bg-gradient-to-b from-gray-800 via-gray-900 to-black text-white shadow-lg">
      <div className="container mx-auto px-5 py-5 flex flex-col lg:flex-row items-start lg:items-center justify-between">
        <div className="flex items-center space-x-3 mb-4 lg:mb-0">
          <Image
            src="/favicon.ico"
            alt="Logo de la aplicación"
            width={40}
            height={40}
          />
          <h1 className="text-sm sm:text-base lg:text-lg font-bold">
            Cristian Cuartas | Frontend developer
          </h1>
        </div>
        <div className="flex items-center space-x-2 mb-2 lg:mb-0">
          <span
            className={`inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-white ${
              itemCount > 0 ? "bg-blue-600" : "bg-gray-700"
            } rounded-full mr-3`}
          >
            <FaShoppingCart className="text-white text-2xl mr-1" />
            Items: {itemCount}
          </span>
          <span
            className={`inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-white ${
              totalPrice > 0 ? "bg-purple-600" : "bg-gray-700"
            } rounded-full`}
          >
            <FaDollarSign className="text-white text-2xl mr-1" />
            Total: {totalPrice.toFixed(2)}
          </span>
        </div>
        <div className="flex flex-col lg:flex-row items-start lg:items-center space-y-2 lg:space-y-0 space-x-0 lg:space-x-2 w-full lg:w-auto mt-2 lg:mt-0">
          <div className="relative w-full lg:w-auto">
            <select
              id="sortField"
              value={sortField}
              onChange={handleSortChange}
              className="shadow-md rounded-lg bg-gray-700 border border-gray-600 text-white pl-4 pr-10 py-2 focus:outline-none focus:ring-2 focus:ring-blue-600 w-full"
            >
              <option value="">Filter products</option>
              <option value="price">Price</option>
              <option value="rating">Rating</option>
            </select>
          </div>
          <div className="relative w-full lg:w-auto mt-2 lg:mt-0">
            <label htmlFor="search" className="sr-only">
              Search products
            </label>
            <input
              id="search"
              type="search"
              value={searchValue}
              onChange={handleSearchChange}
              aria-label="Campo de búsqueda"
              placeholder="Search products"
              className="shadow-md rounded-lg w-full lg:w-96 pl-10 pr-3 py-2 text-white bg-gray-700 focus:outline-none focus:ring-2 focus:ring-blue-600 border border-gray-600 placeholder-gray-300"
            />
            <FaSearch className="absolute left-3 top-1/2 transform -translate-y-1/2 text-gray-300" />
          </div>
        </div>
      </div>
    </header>
  );
};
