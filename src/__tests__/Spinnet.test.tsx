import React from "react";
import { render } from "@testing-library/react";

import "@testing-library/jest-dom/extend-expect";

import { Spinner } from "../components/Spinner";

describe("Spinner", () => {
  it("should render the spinner and loading text", () => {
    const { getByText } = render(<Spinner />);

    expect(getByText("Cargando más productos...")).toBeInTheDocument();
  });
});
