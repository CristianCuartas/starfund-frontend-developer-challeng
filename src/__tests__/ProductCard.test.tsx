import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import cartReducer, { addToCart, removeFromCart } from "../../store/cartSlice";

import "@testing-library/jest-dom/extend-expect";

import ProductCard from "../components/ProductCard";

const product = {
  id: 1,
  title: "Test Product",
  description: "This is a test product",
  image: "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
  price: 100,
  category: "any",
  currency: "USD",
  rating: {
    rate: 4.5,
    count: 1,
  },
};

describe("ProductCard", () => {
  it("should render the product details", () => {
    const store = configureStore({
      reducer: {
        cart: cartReducer,
      },
    });

    const { getByText, getByAltText } = render(
      <Provider store={store}>
        <ProductCard product={product} />
      </Provider>
    );

    expect(getByText("Test Product")).toBeInTheDocument();
    expect(getByText("This is a test product")).toBeInTheDocument();
    expect(getByText("$100 USD")).toBeInTheDocument();
    expect(getByAltText("Imagen de Test Product")).toBeInTheDocument();
  });

  it("should handle adding and removing from cart", () => {
    const store = configureStore({
      reducer: {
        cart: cartReducer,
      },
    });

    const { getByText } = render(
      <Provider store={store}>
        <ProductCard product={product} />
      </Provider>
    );

    const addToCartButton = getByText("Add to Cart");

    fireEvent.click(addToCartButton);

    expect(store.getState().cart.items).toHaveLength(1);
    expect(getByText("Remove from Cart")).toBeInTheDocument();

    fireEvent.click(getByText("Remove from Cart"));

    expect(store.getState().cart.items).toHaveLength(0);
    expect(getByText("Add to Cart")).toBeInTheDocument();
  });
});
