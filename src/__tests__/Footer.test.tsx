import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { Footer } from "../components/Footer";

describe("Footer", () => {
  it("should render the footer with the correct year", () => {
    const currentYear = new Date().getFullYear();

    const { getByText } = render(<Footer />);

    expect(
      getByText(
        `© ${currentYear} - Front End Developer Challenge. Todos los derechos reservados.`
      )
    ).toBeInTheDocument();
  });

  it("should render the link to the LinkedIn profile", () => {
    const { getByText } = render(<Footer />);

    const linkElement = getByText("/in/dexhcz/");

    expect(linkElement).toBeInTheDocument();
    expect(linkElement.closest("a")).toHaveAttribute(
      "href",
      "https://www.linkedin.com/in/dexhcz/"
    );
  });
});
