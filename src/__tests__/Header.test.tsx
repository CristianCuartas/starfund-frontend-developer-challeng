import React from "react";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import { render, fireEvent } from "@testing-library/react";

import "@testing-library/jest-dom/extend-expect";

import { Header } from "../components/Header";
import sortReducer from "../../store/sortSlice";
import cartReducer from "../../store/cartSlice";
import searchReducer from "../../store/searchSlice";

it("should render the header component", () => {
  const store = configureStore({
    reducer: {
      search: searchReducer,
      sort: sortReducer,
      cart: cartReducer,
    },
  });

  const { getByText } = render(
    <Provider store={store}>
      <Header />
    </Provider>
  );

  expect(
    getByText("Cristian Cuartas | Frontend developer")
  ).toBeInTheDocument();
});

it("should render the search and sort fields", () => {
  const store = configureStore({
    reducer: {
      search: searchReducer,
      sort: sortReducer,
      cart: cartReducer,
    },
  });

  const { getByLabelText } = render(
    <Provider store={store}>
      <Header />
    </Provider>
  );

  expect(getByLabelText("Buscar producto")).toBeInTheDocument();
  expect(getByLabelText("Sort by:")).toBeInTheDocument();
});

it("should handle changes to the search and sort fields", () => {
  const store = configureStore({
    reducer: {
      search: searchReducer,
      sort: sortReducer,
      cart: cartReducer,
    },
  });

  const { getByLabelText } = render(
    <Provider store={store}>
      <Header />
    </Provider>
  );

  fireEvent.change(getByLabelText("Buscar producto"), {
    target: { value: "test" },
  });
  fireEvent.change(getByLabelText("Sort by:"), { target: { value: "price" } });

  expect(store.getState().search.text).toBe("test");
  expect(store.getState().sort.field).toBe("price");
});
