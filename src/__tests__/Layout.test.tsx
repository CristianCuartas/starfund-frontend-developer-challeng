import React from "react";
import { render } from "@testing-library/react";

import "@testing-library/jest-dom/extend-expect";

import { Layout } from "../components/Layout";

describe("Layout", () => {
  it("should render its children", () => {
    const { getByText } = render(
      <Layout>
        <div>Child Component</div>
      </Layout>
    );

    expect(getByText("Child Component")).toBeInTheDocument();
  });
});
