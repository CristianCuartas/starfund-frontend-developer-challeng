import { createSlice, PayloadAction, createSelector } from "@reduxjs/toolkit";
import { Product } from "@/types";

interface CartState {
  items: Product[];
}

const initialState: CartState = {
  items: [],
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, action: PayloadAction<Product>) => {
      state.items.push(action.payload);
    },
    removeFromCart: (state, action: PayloadAction<number>) => {
      state.items = state.items.filter((item) => item.id !== action.payload);
    },
  },
});

export const { addToCart, removeFromCart } = cartSlice.actions;

export const isProductInCart = createSelector(
  (state: CartState) => state.items,
  (_: CartState, productId: number) => productId,
  (items, productId) => items.some((item) => item.id === productId)
);

export default cartSlice.reducer;
