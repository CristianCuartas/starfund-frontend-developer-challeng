import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface SortState {
  field: "" | "price" | "rating";
}

const initialState: SortState = {
  field: "",
};

const sortSlice = createSlice({
  name: "sort",
  initialState,
  reducers: {
    setSortField: (state, action: PayloadAction<"" | "price" | "rating">) => {
      state.field = action.payload;
    },
  },
});

export const { setSortField } = sortSlice.actions;

export default sortSlice.reducer;
