import { configureStore } from "@reduxjs/toolkit";
import searchReducer from "./searchSlice";
import sortReducer from "./sortSlice";
import cartReducer from "./cartSlice";

const store = configureStore({
  reducer: {
    sort: sortReducer,
    cart: cartReducer,
    search: searchReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export default store;
