# Next.js eCommerce Platform

This project is an eCommerce platform built with [Next.js](https://nextjs.org/), [TypeScript](https://www.typescriptlang.org/), [Redux](https://redux.js.org/) and [Tailwind CSS](https://tailwindcss.com/). It provides a user interface for browsing products, adding them to the shopping cart, and filtering and sorting products by various criteria.

![Project Image](project-overview.png)

## Demo

Here you can see a working demo of the project: [Link to demo](https://starfund-frontend-developer-challenge.vercel.app/)

## Features

- Displays products in a grid format, loading new products with infinite scroll.
- Allows to search products by title in real time.
- Allows to sort products by price or rating.
- Allows you to add products to a shopping cart.
- Displays the total number of items and total price in a fixed header.
- Responsive design with Tailwind CSS.
- Status management with Redux.

### Installation

To install the project, clone the repository and run ``yarn`:

`````bash
git clone https://gitlab.com/CristianCuartas/starfund-frontend-developer-challeng
cd starfund-frontend-developer-challeng
yarn
```

## Quickstart

To run the project locally, run:

````bash
yarn dev
```

## Tests

To run the tests, use the command:

````bash
yarn test
```

### Prerequisites

- Node.js 12+
- yarn
`````
