/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
        "gradient-radial-at-t":
          "radial-gradient(at top, var(--tw-gradient-stops))",
      },
      colors: {
        darkKhaki: "#663399",
        steelBlue: "#4682B4",
      },
      spacing: {
        customLg: "6rem",
        custom: "15rem",
      },
    },
  },
  plugins: [],
};
